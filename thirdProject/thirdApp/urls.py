from django.conf.urls import url
from thirdApp import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^showData', views.show)
]
