from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
    return HttpResponse("Hello third project")

def show(request):
    my_dict = {'a': "hello heyhey"}
    return render(request, 'thirdApp/index.html', context = my_dict)
